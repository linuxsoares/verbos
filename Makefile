BASE_SETTINGS=verbos.settings.
DEV_SETTINGS=verbos.settings.development
TEST_SETTINGS=verbos.settings.test
PRODUCTION_SETTINGS=verbos.settings.production

clean:
	find . -name "*.pyc" | xargs rm -rf
	find . -name "*.pyo" | xargs rm -rf
	find . -name "__pycache__" -type d | xargs rm -rf
	rm -f .coverage
	rm -rf htmlcov/
	rm -f coverage.xml
	rm -f *.log

install:
	pip install -r requirements/dep.txt

install-test:
	pip install -r requirements/test.txt

install-dev:
	pip install -r requirements/development.txt

superuser:
	./manage.py createsuperuser --settings=$(DEV_SETTINGS)

run:
	DJANGO_SETTINGS_MODULE=$(PRODUCTION_SETTINGS) gunicorn -b 0.0.0.0:8000 -w 1 verbos.wsgi

run-django:
	DJANGO_SETTINGS_MODULE=$(DEV_SETTINGS) ./manage.py runserver

makemigrations:
	./manage.py makemigrations --settings=$(DEV_SETTINGS)

migrate:
	./manage.py migrate --settings=$(DEV_SETTINGS)

shell:
	DJANGO_SETTINGS_MODULE=$(DEV_SETTINGS) ./manage.py shell_plus
