from django.contrib import admin
from api.models import ConjugacaoVerbal, Verbo


class ConjugacaoVerbalAdmin(admin.ModelAdmin):
    list_filter = ('nome_conjulgacao',)


class VerboAdmin(admin.ModelAdmin):
    pass


admin.site.register(ConjugacaoVerbal, ConjugacaoVerbalAdmin)
admin.site.register(Verbo, VerboAdmin)
