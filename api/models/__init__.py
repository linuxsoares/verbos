from api.models.verbos import (
    ConjugacaoVerbal,
    LogInsert,
    LogVerbos,
    TempoVerbal,
    TipoVerbal,
    Verbo
)

__all__ = [
    'ConjugacaoVerbal',
    'LogInsert',
    'LogVerbos',
    'TempoVerbal',
    'TipoVerbal',
    'Verbo',
]
