from django.db import models


class ConjugacaoVerbal(models.Model):
    id_conjulgacao = models.IntegerField(
        primary_key=True
    )
    nome_conjulgacao = models.CharField(blank=True, max_length=100)
    tempo_verbal = models.ForeignKey(
        'TempoVerbal',
        on_delete=models.CASCADE,
        related_name='tempo_verbal'
    )
    verbo = models.ForeignKey('Verbo', on_delete=models.CASCADE)

    @property
    def tempo_verbal_nome(self):
        return self.tempo_verbal.nome

    @property
    def tipo_verbal_nome(self):
        return self.tempo_verbal.tipo_verbo.nome

    def __str__(self):
        return '{} {}'.format(
            self.id_conjulgacao,
            self.nome_conjulgacao,
        )

    def get_for_verb(palavra):
        return ConjugacaoVerbal.objects.filter(
            verbo__nome=palavra.lower()
        ).order_by('tempo_verbal__id_tempo_verbal', 'id_conjulgacao')


class LogInsert(models.Model):
    insert_status = models.CharField(blank=True, max_length=100)
    nome = models.CharField(blank=True, max_length=100)


class LogVerbos(models.Model):
    log_verbo = models.CharField(blank=True, max_length=100)
    nome_verbo = models.CharField(blank=True, max_length=100)
    quantidade = models.IntegerField(blank=True, null=True)


class TempoVerbal(models.Model):
    id_tempo_verbal = models.IntegerField(
        primary_key=True
    )
    nome = models.CharField(blank=True, max_length=100)
    tipo_verbo = models.ForeignKey('TipoVerbal', on_delete=models.CASCADE)

    def __unicode__(self):
        return self.nome

    def __repr__(self):
        return '{}-{}'.format(self.id_tempo_verbal, self.nome)


class TipoVerbal(models.Model):
    id_tipo_verbal = models.IntegerField(
        primary_key=True
    )
    nome = models.CharField(blank=True, max_length=100)


class Verbo(models.Model):
    nome = models.CharField(
        blank=True,
        max_length=255,
        db_index=True
    )

    def get_for_letter(letra):
        return Verbo.objects.filter(
            nome__startswith=letra.lower()
        )

    def get_all():
        return Verbo.objects.all()
