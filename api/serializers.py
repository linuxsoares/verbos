from rest_framework import serializers
from api.models.verbos import Verbo, ConjugacaoVerbal


class VerboSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Verbo
        fields = (
            'id',
            'nome',
        )


class ConjugacaoVerbalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ConjugacaoVerbal
        fields = (
            'id_conjulgacao',
            'nome_conjulgacao',
            'tempo_verbal_nome',
            'tipo_verbal_nome'
        )
