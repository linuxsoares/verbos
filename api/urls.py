from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from api.views import VerboView, VerboConjugadoView
from api import views

# router = routers.DefaultRouter()
# router.register(r'lista', VerboView)

urlpatterns = [
    url(r'^$', views.VerboView.as_view()),
    url(r'^(?P<letra>[\w|\W])/$', views.VerboView.as_view()),
    url(
        r'^(?P<palavra>[\w|\W]+)/conjugado/$',
        views.VerboConjugadoView.as_view(),
    ),
]
