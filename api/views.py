from rest_framework.views import APIView
from rest_framework.response import Response
from api.models.verbos import (
    Verbo as verbo,
    ConjugacaoVerbal as conjug
)
from api.serializers import VerboSerializer, ConjugacaoVerbalSerializer
from logging import getLogger

loggger = getLogger(__file__)


class VerboView(APIView):
    charset = 'utf-8'

    def get(self, request, letra=None):
        if letra:
            loggger.info('Get verbos por letra. {}'.format(letra))
            verbos = verbo.get_for_letter(letra)
        else:
            loggger.info('Get verbos.')
            verbos = verbo.get_all()
        serializers = VerboSerializer(verbos, many=True)
        return Response(serializers.data)


class VerboConjugadoView(APIView):
    charset = 'utf-8'

    def get(self, request, palavra):
        if palavra:
            loggger.info('Get verbo {}.'.format(palavra))
            verbo = conjug.get_for_verb(
                palavra=palavra
            )
            serializers = ConjugacaoVerbalSerializer(
                verbo,
                many=True,
                context={'request': request}
            )
            return Response(serializers.data)
        return Response({})
