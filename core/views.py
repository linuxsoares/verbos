import textwrap

from django.http import HttpResponse
from django.views.generic.base import View


class HomePageView(View):

    def dispatch(request, *args, **kwargs):
        response = textwrap.dedent(
            '''
            <html>
                <head>
                    <title>Verbos Rest Service</title>
                </head>
                <body>
                    <h1>Verbos Rest Service</h1>
                    <p>Acesse o link abaixo para ver a documentação:</p>
                    <li>
                        <a href="/api/docs">
                            Documentação Swagger - API VERBOS
                        </a>
                    </li>
                </body>
            </html>
            '''
        )
        return HttpResponse(response)
