from verbos.settings.base import *  # noqa

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True
DEBUG = False
