from django.conf.urls import url, include
from django.contrib import admin
from api import urls as api_urls
from core.views import HomePageView
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Verbos Rest Service')

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^api/docs$', schema_view, name='doc'),
    url(r'^admin/', admin.site.urls),
    url(r'^verbo/', include(api_urls)),
    url(
        r'^api-auth/', include(
            'rest_framework.urls', namespace='rest_framework'
        )
    )
]
